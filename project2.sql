DROP DATABASE IF EXISTS project2db;

CREATE DATABASE project2db;

USE project2db;

CREATE TABLE admin
(
idAdmin INTEGER NOT NULL AUTO_INCREMENT,
adminName varchar(45) NOT NULL,
adUsername varchar(45) NOT NULL,
adPassword varchar(45) NOT NULL,
adDOB varchar(45) NOT NULL,
adEmail varchar(45) NOT NULL,
adPhone INTEGER NOT NULL,
adAddress1 varchar(45) NOT NULL,
adAddress2 varchar(45) DEFAULT NULL,
adTownCity varchar(45) NOT NULL,
adGender CHAR NOT NULL,
  
PRIMARY KEY (idAdmin),
UNIQUE KEY (adUsername)

) 
ENGINE=InnoDB;

CREATE TABLE member 
(
idMember INTEGER NOT NULL AUTO_INCREMENT,
mName varchar(45) NOT NULL,
mUsername varchar(45) NOT NULL,
mPassword varchar(45) NOT NULL,
mDOB date NOT NULL,
mEmail varchar(45) NOT NULL,
mPhone varchar(45) NOT NULL,
mAddress1 varchar(45) NOT NULL,
mAddress2 varchar(45) DEFAULT NULL,
mTownCity varchar(45) NOT NULL,
mGender CHAR NOT NULL,
mStatus INTEGER NOT NULL,
mActive bit(1) NOT NULL,
mFines double(3,2) NOT NULL,
  
PRIMARY KEY (idMember),
UNIQUE KEY (mUsername,mPhone)

) 
ENGINE=InnoDB;

CREATE TABLE films
(
idFilms INTEGER NOT NULL AUTO_INCREMENT,
Title varchar(45) NOT NULL,
Genre varchar(45) NOT NULL,
Synopsis varchar(45) NOT NULL,  

PRIMARY KEY (idFilms)

) 
ENGINE=InnoDB;

CREATE TABLE copies 
(
copyId INTEGER NOT NULL AUTO_INCREMENT,
idFilms INTEGER NOT NULL,
numCopies INTEGER NOT NULL,
  
PRIMARY KEY (copyId),
/*KEY idFilms_idx (idFilms)*/
FOREIGN KEY (idFilms) REFERENCES films(idFilms) ON UPDATE CASCADE ON DELETE CASCADE
) 
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS loan
(
idLoan INTEGER NOT NULL AUTO_INCREMENT,
idMember INTEGER NOT NULL,
idFilms INTEGER NOT NULL,
Title VARCHAR(23),
Borrowed timestamp NOT NULL,

PRIMARY KEY (idLoan),
/*KEY idMember_idx (idMember),*/
FOREIGN KEY (idMember) REFERENCES member(idMember) ON UPDATE CASCADE ON DELETE CASCADE, 
/*KEY idFilms_idx (idFilms)*/
FOREIGN KEY (idFilms) REFERENCES films(idFilms) ON UPDATE CASCADE ON DELETE CASCADE
) 
ENGINE=InnoDB;

CREATE TABLE errorlog
(	
id INTEGER NOT NULL AUTO_INCREMENT,	
description VARCHAR(100),	
PRIMARY KEY(id)
) 
ENGINE=InnoDB;

CREATE TABLE payment
(	
memberId INTEGER NOT NULL AUTO_INCREMENT,	
payFine Double(3,2),	
PRIMARY KEY(memberId),
FOREIGN KEY (memberId) REFERENCES member(idMember) ON UPDATE CASCADE ON DELETE CASCADE
) 
ENGINE=InnoDB;

DROP TRIGGER IF EXISTS addCopies;
DELIMITER //
CREATE TRIGGER addCopies AFTER INSERT ON films
FOR EACH ROW
BEGIN
INSERT INTO copies ( idFilms,numCopies )
VALUES ( NEW.idFilms, 5 );
END
//
DELIMITER ;

USE project2db;
--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idAdmin`, `adminName`, `adUsername`, `adPassword`, `adDOB`, `adEmail`, `adPhone`, `adAddress1`, `adAddress2`, `adTownCity`, `adGender`) VALUES
(1, 'Jason', 'Jelde', 'password', '1985-08-27', 'jason@hotmail.com', 123456, 'MyHouse', 'MyStreet', 'MyTown', '?'),
(2, 'Sarah', 'SbickM', '12345', '1987-05-08', 'sarah@gmail.com', 987456, 'MyFlat', 'MyRoad', 'MyCity', 'O');


--
-- Dumping data for table `films`
--

INSERT INTO `films` (`idFilms`, `Title`, `Genre`, `Synopsis`) VALUES
(1, 'Blade Runner', 'Science Fiction', 'Film Noir set in the future'),
(2, 'Dawn of the Dead', 'Horror', 'Zombies take over the World'),
(3, 'Goodfellas', 'Thriller', 'Life in the Mafia'),
(4, 'The Running Man', 'Action', 'Game show of the Future'),
(5, 'Aliens', 'Science Fiction', 'Half sci fi half action'),
(6, 'Masters of the Universe', 'Fantasy', 'Based on the eighties cartoon'),
(7, 'The Empire Strikes Back', 'Science Fiction', 'Sci-Fi Classic'),
(8, 'Dirty Dancing', 'Romance', 'A bunch of people dancing'),
(9, 'The Searchers', 'Western', 'An uncle searches for his neice');

--
-- Dumping data for table `copies`
--

INSERT INTO `copies` (`copyId`, `idFilms`, `numCopies`) VALUES
(0, 1, 1),
(1, 2, 4),
(2, 3, 4),
(3, 4, 4),
(4, 5, 4),
(5, 6, 4),
(6, 7, 4),
(7, 8, 4),
(8, 9, 4),
(9, 10, 4);


--
-- Dumping data for table `member`
--

INSERT INTO `member` (`idMember`, `mName`, `mUsername`, `mPassword`, `mDOB`, `mEmail`, `mPhone`, `mAddress1`, `mAddress2`, `mTownCity`, `mGender`, `mStatus`, `mActive`, `mFines`) VALUES
(1, 'David McQuaid', 'dmak', 'audrey', '1975-08-09', 'david.dmcq@gmail.com', '0867356329', '40a Old Muirhevna', 'Dublin Rd', 'Dundalk', 'm', 6, 1, 10.00),
(2, 'Eoin Healy', 'eheal', '12345', '1991-08-15', 'healy5150@hotmail.com', '0871794331', 'Carntown', 'Ballymakenny', 'Drogheda', 'M', 6, 1, 2.00),
(3, 'Jason Elder', 'elder', '11111', '1985-08-27', 'jason-elder@gmail.com', '0871234567', '41 Ashbrook', 'Avenue Road', 'Dundalk', 'M', 4, 0, 0.00),
(4, 'Sarah Bickmore', 'sarah', '00000', '1987-05-28', 'sbickmore@hotmail.com', '0871234567', 'Willowdale', 'Bay Estate', 'Dundalk', 'F', 0, -1, 0.00);


--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`idLoan`, `idMember`, `idFilms`, `Title`, `Borrowed`) VALUES
(1,1,1,'Blade Runner','2014-04-20'),
(2,2,9,'Dawn of the Dead','2014-04-28'); 


--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`memberId`, `payFine`) VALUES
(1, 4.00);