USE project2db;

DROP PROCEDURE IF EXISTS returnFilm;

DELIMITER //
CREATE PROCEDURE returnFilm
( 
IN loanId INTEGER, 
OUT bill INTEGER,
OUT success BOOLEAN
)

BEGIN

DECLARE timeDiff INTEGER; 

DECLARE workF INTEGER;

DECLARE EXIT HANDLER FOR sqlwarning, sqlexception 
BEGIN
	INSERT INTO errorlog ( description ) VALUES
	(CONCAT("Failed insert for loan id = ",loanId, " on ",CURDATE()," at ",CURTIME()));

    rollback;
END;

START TRANSACTION;

SELECT @borrow:=Borrowed FROM loan WHERE idLoan = loanId;

SELECT TIMESTAMPDIFF(DAY,@borrow,CURRENT_DATE()) INTO timeDiff;

IF timeDiff <= 7 THEN

	SET bill = 0;

ELSE if timeDiff > 7 THEN

	SET workF = timeDiff - 7;
	SET bill = 2 * workF;

END IF;
END IF;

SELECT @memId:=idMember FROM loan WHERE idLoan = loanId;

UPDATE member SET mFines = bill WHERE idMember = @memId;

SELECT @filmId:= idFilms FROM loan WHERE idLoan = loanId;

SELECT @numC:=numCopies FROM copies WHERE idFilms = @filmId;

SET @numC = @numC + 1;

UPDATE copies SET numCopies = @numC WHERE idFilms = @filmId;

DELETE FROM loan WHERE idLoan = loanId;

COMMIT;


SET success = TRUE;

END //
DELIMITER ;




DROP PROCEDURE IF EXISTS removeFilm;

DELIMITER //
CREATE PROCEDURE removeFilm
( 
IN idFilms INTEGER,
OUT success BOOLEAN
)

BEGIN

DECLARE EXIT HANDLER FOR sqlwarning, sqlexception 
BEGIN
	INSERT INTO errorlog ( description ) VALUES
	(CONCAT("Failed delete for film id = ",filmId, " on ",CURDATE()," at ",CURTIME()));

    rollback;
END;

SET success = false;

START TRANSACTION;

SELECT @theCount:=COUNT(idFilms) FROM loan WHERE idFilms=filmId;

IF @theCount = 0 THEN

	DELETE FROM films WHERE idFilms = filmId;
	DELETE FROM copies WHERE idFilms = filmId;
	SET success = TRUE;

ELSE 
	
	SET success = FALSE;

END IF;

COMMIT;


END //
DELIMITER ;
